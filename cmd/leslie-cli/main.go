package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/user"
)

type Color string

type PackageJSON struct {
	Name        string      `json:"name"`
	Version     string      `json:"version"`
	Description string      `json:"description"`
	Main        string      `json:"main"`
	Scripts     Scripts     `json:"scripts"`
	Author      string      `json:"author"`
	License     string      `json:"license"`
	Dependecies Dependecies `json:"dependencies"`
}

type Scripts struct {
	Start string `json:"start"`
	Test  string `json:"test"`
}

type Dependecies struct {
	Jest string `json:"jest"`
}

const (
	ColorBlack  Color = "\u001b[30m"
	ColorRed          = "\u001b[31m"
	ColorGreen        = "\u001b[32m"
	ColorYellow       = "\u001b[33m"
	ColorBlue         = "\u001b[34m"
	ColorReset        = "\u001b[0m"
)

var (
	appDir string
)

func getHomePath() string {
	usr, _ := user.Current()
	return usr.HomeDir
}

func colorize(color Color, message string) {
	fmt.Println(string(color), message, string(ColorReset))
}

func validation() string {
	appName := flag.String("name", "", "Custom app name to create the scaffolding")
	flag.Parse()

	if *appName == "" {
		flag.PrintDefaults()
		exitWithError("try -name=appName", errors.New("[Error] missing app name"))
		os.Exit(1)
	}

	return *appName
}

func finishedInstructions(appName string) {
	fmt.Println()
	colorize(ColorGreen, "Done!")
	fmt.Println()
	colorize(ColorYellow, "1) cd "+getHomePath()+"/"+appName)
	colorize(ColorYellow, "2) npm install")
	colorize(ColorYellow, "3) npm start")
	colorize(ColorYellow, "4) npm run test (to run unit testing)")
	os.Exit(0)
}

func exitWithError(message string, err error) {
	fmt.Println(err)
	colorize(ColorRed, message)
	os.Exit(1)
}

func copyFile(file string) error {
	from, err := os.Open("templates/" + file)
	if err != nil {
		return err
	}
	defer from.Close()

	to, err := os.OpenFile(appDir+"/"+file, os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		return err
	}
	defer to.Close()

	_, err = io.Copy(to, from)
	if err != nil {
		return err
	}

	return nil
}

func setScaffolder(appDir string, appName string) {
	colorize(ColorBlue, "Generating scaffolding on "+appDir)
	// Creo carpeta base
	colorize(ColorBlue, "Creating base folder")
	if _, err := os.Stat(appDir); os.IsNotExist(err) {
		os.Mkdir(appDir, os.ModePerm)
	}

	// Copio archivo jest.config.js a directorio
	colorize(ColorBlue, "Copying jest.config.js")
	err := copyFile("jest.config.js")
	if err != nil {
		exitWithError("Error copying jest.config.js", err)
	}

	// Creo package.json
	colorize(ColorBlue, "Creating package.json")
	packageJson := &PackageJSON{
		Name:        appName,
		Version:     "1.0.0",
		Description: "Basic app created by leslie cli :)",
		Main:        "index.js",
		Scripts: Scripts{
			Start: "node index.js",
			Test:  "jest -f index.spec.js",
		},
		Author:  "leslie-cli",
		License: "ISC",
		Dependecies: Dependecies{
			Jest: "^26.3.0",
		},
	}
	packageJsonFile, err := json.MarshalIndent(packageJson, "", "\t")
	if err != nil {
		exitWithError("Error on MarshalIndent package.json", err)
	}
	err = ioutil.WriteFile(appDir+"/package.json", packageJsonFile, 0644)
	if err != nil {
		exitWithError("Error on writing package.json", err)
	}

	// Copio archivo index.js a directorio
	colorize(ColorBlue, "Copying index.js")
	err = copyFile("index.js")
	if err != nil {
		exitWithError("Error copying index.js", err)
	}

	// Copio archivo index.spec.js a directorio
	colorize(ColorBlue, "Copying index.spec.js")
	err = copyFile("index.spec.js")
	if err != nil {
		exitWithError("Error copying index.spec.js", err)
	}
}

func main() {
	// Valido que tenga app name
	appName := validation()
	colorize(ColorGreen, "Starting Leslie Cli <3")
	appDir = getHomePath() + "/" + appName
	setScaffolder(appDir, appName)
	finishedInstructions(appName)
	os.Exit(0)
}
