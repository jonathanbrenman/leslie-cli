/* 
    Jest unit testing:
    Documentation: https://jestjs.io/docs/en/getting-started
*/

const myFunction = require('./index');

test('Function main should return hola mundo', () => {
    expect(myFunction.main()).toBe("hola mundo");
});