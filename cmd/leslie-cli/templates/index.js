/* 
    Basic function file with return
*/

const main = () => {
    return "hola mundo"
}

let result = main();
console.log(result);

module.exports.main = main;